if (document.querySelector('.product')) {
	// ^ to get rid of error on pages where Vue.js isn't being used

	for (product of document.querySelectorAll('.product')) {
		new Vue({
			el: product,
			data: {
				quantity: 0
			},
			methods: {
				incrementQuantity: function () {
					this.quantity += 1;
				},
				decrementQuantity: function () {
					this.quantity -= 1;
					if (this.quantity < 0)
						this.quantity = 0;
				}
			}
		})
	}

	for (modalExtra of document.querySelectorAll('.modal-extra')) {
		new Vue({
			el: modalExtra,
			data: {
				quantity: 0,
				total: 0,
				price: modalExtra.firstElementChild.nextElementSibling.nextElementSibling.dataset.price
			},
			methods: {
				incrementQuantityAndPrice: function () {
					this.quantity += 1;
					this.price = parseInt(this.price);
					this.total += this.price;
				},
				decrementQuantityAndPrice: function () {
					this.quantity -= 1;
					this.price = parseInt(this.price);
					this.total -= this.price;
					if (this.total < 0)
						this.total = 0;
					if (this.quantity < 0)
						this.quantity = 0;
				}
			}
		})
	}

	new Vue({
		el: '.modal .n-and-add .n',
		data: {
			quantity: 0
		},
		methods: {
			incrementQuantity: function () {
				this.quantity += 1;
			},
			decrementQuantity: function () {
				this.quantity -= 1;
				if (this.quantity < 0)
					this.quantity = 0;
			}
		}
	})
}

new Vue({
	el: '#header',
	data: {
		menuActive: false
	},
	methods: {
		toggleMenu: function() {
			this.menuActive = !this.menuActive;
		}
	}
})

var dim = document.querySelector('.dim');

for (e of document.querySelectorAll('.product .info-and-img')) {
	e.onclick = function () {
		var modal = document.querySelector('.modal');
		dim.classList.add('visible');
		modal.style.display = 'block';
		document.querySelector('.three-icons .header-menu-wrapper').style.zIndex = '1';
	}
}

if (document.querySelector('.modal .close')) {
	document.querySelector('.modal .close').onclick = function () {
		dim.classList.remove('visible');
		this.parentElement.style.display = 'none';
		document.querySelector('.three-icons .header-menu-wrapper').style.zIndex = '2';
	}
}

if(document.querySelector('.filters-slider')) {
	var flkty = new Flickity('.filters-slider', {
		cellAlign: 'left',
		contain: true,
		prevNextButtons: false,
		groupCells: true
	});
}

// document.querySelector('.header-menu-wrapper .ham').onclick = function () {
// 	dim.classList.toggle('visible');
// 	this.parentElement.classList.toggle('visible');
// 	document.querySelector('.three-icons:last-of-type .icon-with-text:first-child').classList.toggle('hidden');
// }

// window.onresize = function () {
// 	let is729 = window.matchMedia('(min-width: 729px)')
// 	var headerMenuWrapper = document.querySelector('.header-menu-wrapper');
// 	if (is729.matches && headerMenuWrapper.classList.contains('visible')) {
// 		dim.classList.remove('visible');
// 		headerMenuWrapper.classList.remove('visible');
// 		document.querySelector('.three-icons:last-of-type .icon-with-text:first-child').classList.remove('hidden');
// 	}
// }
var targets = document.querySelectorAll('.anim');

let observerOptions = {
	root: null,
	rootMargin: '0px',
	threshold: 0.7
}

var foodAnimationToggled = false;

observer = new IntersectionObserver((entries) => {
	entries.forEach(entry => {
		if (entry.intersectionRatio >= 0.7) {
			console.log(entry.intersectionRatio);
			if (entry.target.classList.contains('pics')) {
				if (!foodAnimationToggled) {
					document.querySelector('.food').style.animationName = 'food-slide-in';
					document.querySelector('.goodies').style.animationName = 'goodies-slide-in';
					document.querySelector('.weed').style.animationName = 'slide-from-top';
					foodAnimationToggled = true;
				}
				$('.snap-buttons div').removeClass('active');
				$('.snap-buttons div:nth-child(1)').addClass('active');

				setTimeout(() => {
					document.querySelector('.food').style.animation = 'circle1 30s linear infinite';
					document.querySelector('.weed').style.animation = 'circle2 30s linear infinite';
					document.querySelector('.goodies').style.animation = 'circle3 40s linear infinite';
				}, 1500);
			}

			if (entry.target.classList.contains('container')) {
				entry.target.firstElementChild.style.animationName = 'slide-from-left';
				entry.target.lastElementChild.style.animationName = 'slide-from-right';
			}

			if (entry.target.classList.contains('one')) {
				$('.snap-buttons div').removeClass('active');
				$('.snap-buttons div:nth-child(3)').addClass('active');
			}

			if (entry.target.classList.contains('two')) {
				$('.snap-buttons div').removeClass('active');
				$('.snap-buttons div:nth-child(4)').addClass('active');
			}

			if (entry.target.classList.contains('three')) {
				$('.snap-buttons div').removeClass('active');
				$('.snap-buttons div:nth-child(5)').addClass('active');
			}

			if (entry.target.classList.contains('phones')) {
				entry.target.firstElementChild.firstElementChild.lastElementChild.style.animationName = 'fadein-from-left';
				entry.target.firstElementChild.firstElementChild.nextElementSibling.firstElementChild.style.animationName = 'fadein-from-right';
				for (e of entry.target.querySelectorAll('.left .icon-and-text img:last-child')) {
					e.style.animationName = 'icon-outer-fadein-from-left';
					$('.snap-buttons div').removeClass('active');
					$('.snap-buttons div:nth-child(2)').addClass('active');
				}

				for (e of entry.target.querySelectorAll('.right .icon-and-text img:last-child')) {
					e.style.animationName = 'icon-outer-fadein-from-right';
				}

				for (e of entry.target.querySelectorAll('.left .icon-and-text img:first-child')) {
					e.style.animationName = 'fill-from-left';
				}


				for (e of entry.target.querySelectorAll('.right .icon-and-text img:first-child')) {
					e.style.animationName = 'fill-from-right';
				}

				for (e of entry.target.querySelectorAll('.left .text')) {
					e.style.animationName = 'slide-from-left';
				}

				for (e of entry.target.querySelectorAll('.right .text')) {
					e.style.animationName = 'slide-from-right';
				}
			}
		}
	})
}, observerOptions)

targets.forEach(target => {
	observer.observe(target);
});

function scrollToElement(element) {
	$('html, body').animate({
		scrollTop: $(element).offset().top
	}, 1000);
}

$('.snap-buttons div').on('click', function () {
	$('.snap-buttons div').removeClass('active');
	$(this).addClass('active');
});

$('.snap-buttons div:first-child').on('click', function () {
	scrollToElement('.foods');
});

$('.snap-buttons div:nth-child(2)').on('click', function () {
	scrollToElement('.phones');
});

$('.snap-buttons div:nth-child(3)').on('click', function () {
	scrollToElement('.smart-stuff .container:first-child');
});

$('.snap-buttons div:nth-child(4)').on('click', function () {
	scrollToElement('.smart-stuff .container:nth-child(2)');
});

$('.snap-buttons div:nth-child(5)').on('click', function () {
	scrollToElement('.smart-stuff .container:last-child');
});